import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import functionLinearR as fun
import os
path = os.getcwd() + '\data\ex1data1.txt'
# print(path);
data = pd.read_csv(path, header=None, names=['Population', 'Profit'])
# print(data.describe());

# data.plot(kind='scatter', x='Population', y='Profit', figsize=(12, 8))

data.insert(0, 'Ones', 1)

cols = data.shape[1]
X = data.iloc[:, 0:cols - 1]
y = data.iloc[:, cols - 1:cols]

X = np.matrix(X.values)
y = np.matrix(y.values)
theta = np.matrix(np.array([0, 0]))
print(theta)


alpha = 0.01
iters = 1000
g, cost = fun.gradientDescent(X, y, theta, alpha, iters)
print('g = ', g)
x = fun.computeCost(X, y, g)
print('x = ', x)
x = np.linspace(data.Population.min(), data.Population.max(), 100)
print('x = ', x)
f = g[0, 0] + (g[0, 1] * x)
fig, ax = plt.subplots(figsize=(12, 8))
ax.plot(x, f, 'r', label='Prediction')
ax.scatter(data.Population, data.Profit, label='Traning Data')
ax.legend(loc=2)
ax.set_xlabel('Population')
ax.set_ylabel('Profit')
ax.set_title('Predicted Profit vs. Population Size')

fig, ax = plt.subplots(figsize=(12, 8))
ax.plot(np.arange(iters), cost, 'r')
ax.set_xlabel('Iterations')
ax.set_ylabel('Cost')
ax.set_title('Error vs. Training Epoch')
plt.show()
