import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import functionNN as fun
from scipy.io import loadmat
from sklearn.preprocessing import OneHotEncoder
from scipy.optimize import minimize

data = loadmat('data/ex3data1.mat')
X = data['X']
y = data['y']
m = X.shape[0]
a1 = np.insert(X, 0, values=np.ones(m), axis=None)
encoder = OneHotEncoder(sparse=False)
y_onehot = encoder.fit_transform(y)

input_size = 400
hidden_size = 25
num_labels = 10
learning_rate = 1

# randomly initialize a parameter array of the size of the full network's parameters

params = (np.random.random(size=hidden_size * (input_size + 1) +
                           num_labels * (hidden_size + 1)) - 0.5) * 0.25


fmin = minimize(fun=fun.backprop, x0=params, args=(input_size, hidden_size, num_labels, X, y_onehot, learning_rate),
                method='TNC', jac=True, options={'maxiter': 250})

X = np.matrix(X)
theta1 = np.matrix(np.reshape(
    fmin.x[:hidden_size * (input_size + 1)], (hidden_size, (input_size + 1))))
theta2 = np.matrix(np.reshape(
    fmin.x[hidden_size * (input_size + 1):], (num_labels, (hidden_size + 1))))

a1, z2, a2, z3, h = fun.forward_propagate(X, theta1, theta2)
y_pred = np.array(np.argmax(h, axis=1) + 1)

while 1:
    trong = np.random.randint(0, X.shape[0]- 1)
    print("pred",y_pred[trong])
    img = np.reshape(X[trong],(20,20), order='F')  
    plt.figure(figsize=(3,3))
    plt.imshow(img)
    plt.show()
    i = input("nhap q neu muon stop");
    if(i == "q"):
        break
    print(i)



correct = [1 if a == b else 0 for (a, b) in zip(y_pred, y)]
accuracy = (sum(map(int, correct)) / float(len(correct)))
print( 'accuracy = {0}%'.format(accuracy * 100))

