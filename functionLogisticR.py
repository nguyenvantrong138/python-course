import numpy as np
from scipy.optimize import minimize

def sigmoid(z):
    return 1 /( 1 + np.exp(-z))


def cost(theta, X, y):
    theta = np.matrix(theta)
    X = np.matrix(X)
    y = np.matrix(y)
    g = sigmoid(X * theta.T)
    a = np.multiply(-y, np.log(g))
    b = np.multiply(1 - y, np.log(1 - g))
    return np.sum(a - b) / (len(X))


def costReg(theta, X, y, learningRate):
    theta = np.matrix(theta)
    X = np.matrix(X)
    y = np.matrix(y)
    g = sigmoid(X * theta.T)
    a = np.multiply(-y, np.log(g))
    b = np.multiply((1 - y), np.log(1 - g))
    reg = (learningRate / 2 * len(X)) * np.sum(np.power(theta[:, 1:theta.shape[1]], 2))
    return np.sum(a - b) / (len(X)) + reg
    # first = np.multiply(-y, np.log(sigmoid(X * theta.T)))
    # second = np.multiply((1 - y), np.log(1 - sigmoid(X * theta.T)))
    # reg = (learningRate / 2 * len(X)) * np.sum(np.power(theta[:,1:theta.shape[1]], 2))
    # return np.sum(first - second) / (len(X)) + reg


def gradient(theta, X, y):
    theta = np.matrix(theta)
    X = np.matrix(X)
    y = np.matrix(y)
    param = int(theta.ravel().shape[1])
    grad = np.zeros(param)
    error = (X * theta.T) - y

    for i in range(param):
        term = np.multiply(error, X[:, i])
        grad[i] = np.sum(term) / len(X)

    return grad


def gradientReg(theta, X, y, learningRate):
    theta = np.matrix(theta)
    X = np.matrix(X)
    y = np.matrix(y)
    param = int(theta.ravel().shape[1])
    grad = np.zeros(param)
    error = (X * theta.T) - y
    for i in range(param):
        term = np.multiply(error, X[:, i])
        if(i == 0):
            grad[i] = np.sum(term) / len(X)
        else:
            grad[i] = (np.sum(term) / len(X) +
                       (learningRate / len(X)) * theta[:, i])
    return grad


def gradientPart4(theta, X, y, learningRate):
    theta = np.matrix(theta)
    X = np.matrix(X)
    y = np.matrix(y)
    
    error = sigmoid(X * theta.T) - y
    grad = ((X.T * error) / len(X)).T + ((learningRate / len(X)) * theta)

    grad[0, 0] = np.sum(np.multiply(error, X[:, 0])) / len(X)

    return np.array(grad).ravel()


def predict(theta, X):
    probability = sigmoid(X * theta.T)
    return [1 if x >= 0.5 else 0 for x in probability]

# ///////////////////////////////////////


def one_vs_all(X, y, num_labels, learning_rate):
    rows = X.shape[0]
    params = X.shape[1]

    all_theta = np.zeros((num_labels, params + 1))

    X = np.insert(X, 0, values=np.ones(rows), axis=1)
    for i in range(1, num_labels + 1):
        theta = np.zeros(params + 1)
        y_i = np.array([1 if label == i else 0 for label in y])
        y_i = np.reshape(y_i, (rows, 1))

        # minimize the objective function
        fmin = minimize(fun=costReg, x0=theta, args=(
            X, y_i, learning_rate), method='TNC', jac=gradientPart4)
        all_theta[i - 1, :] = fmin.x

    return all_theta


def predict_all(X, all_theta):  
    rows = X.shape[0]
    params = X.shape[1]
    num_labels = all_theta.shape[0]

    # same as before, insert ones to match the shape
    X = np.insert(X, 0, values=np.ones(rows), axis=1)

    # convert to matrices
    X = np.matrix(X)
    all_theta = np.matrix(all_theta)

    # compute the class probability for each class on each training instance
    h = sigmoid(X * all_theta.T)

    # create array of the index with the maximum probability
    h_argmax = np.argmax(h, axis=1)

    # because our array was zero-indexed we need to add one for the true label prediction
    h_argmax = h_argmax + 1

    return h_argmax
