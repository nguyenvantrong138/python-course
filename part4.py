import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import functionLogisticR as fun
from scipy.io import loadmat
import matplotlib.image as mpimg
data = loadmat('data/ex3data1.mat')
X_data = data['X']
y_data = data['y']
# rows = data['X'].shape[0]
# params = data['X'].shape[1]

# # all_theta = np.zeros((10, params + 1))

# # X = np.insert(data['X'], 0, values=np.ones(rows), axis=1)

# theta = np.zeros(params + 1)

# y_0 = np.array([1 if label == 0 else 0 for label in data['y']])
# y_0 = np.reshape(y_0, (rows, 1))
all_theta = fun.one_vs_all(X_data, y_data, 10, 1)
y_pred = fun.predict_all(X_data, all_theta)

 
while 1:
    trong = np.random.randint(0, X_data.shape[0]- 1)
    print("pred",y_pred[trong])
    img = np.reshape(X_data[trong],(20,20), order='F')  
    plt.figure(figsize=(3,3))
    plt.imshow(img)
    plt.show()
    i = input("nhap q neu muon stop");
    if(i == "q"):
        break
    print(i)


correct = [1 if a == b else 0 for (a, b) in zip(y_pred, data['y'])]  
accuracy = (sum(map(int, correct)) / float(len(correct)))  
print('accuracy = {0}%'.format(accuracy * 100))
